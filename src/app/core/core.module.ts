import { NgModule } from '@angular/core';
import { CarService } from './services/car.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [HttpClientModule],
    providers: [CarService],
})
export class CoreModule { }

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './modules/home/home.component';
import { HeaderComponent } from './core/header/header.component';
import { CoreModule } from './core/core.module';
import { DecimalPipe } from '@angular/common';
import { CarsComponent } from './modules/cars/cars.component';
import { ModalComponent } from './shared/components/modal/modal.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    CarsComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    FormsModule,
    BrowserAnimationsModule ,
    ToastrModule.forRoot()
  ],
  providers: [
    DecimalPipe
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

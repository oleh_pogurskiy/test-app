## Install all packages

Run `nmp i`

## Run json-server
cd src/app/core/mocks -> Run `json-server data.json`

## Run app
Run `ng serve`

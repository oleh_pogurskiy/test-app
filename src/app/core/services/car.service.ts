import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable,  throwError } from 'rxjs';
import { map, catchError, retry } from 'rxjs/operators';

@Injectable()
export class CarService {

  constructor(private http: HttpClient) {}

  getCars(): Observable<any> {
    return this.http.get('http://localhost:3000/cars').pipe(
      map(
        results => results
      ),
      retry(3),
      catchError(
        err => {
          console.log('Error', err.message);
          return throwError(err);
        }
      )
    );
  }

  createCars(data): Observable<any> {
    return this.http.post('http://localhost:3000/cars', data).pipe(
      map(
        results => results
      ),
      retry(3),
      catchError(
        err => {
          console.log('Error', err.message);
          return throwError(err);
        }
      )
    );
  }

  deleteCar(id: number): Observable<any> {
    return this.http.delete(`http://localhost:3000/cars/${id}`).pipe(
      map(
        results => results
      ),
      retry(3),
      catchError(
        err => {
          console.log('Error', err.message);
          return throwError(err);
        }
      )
    );
  }
}

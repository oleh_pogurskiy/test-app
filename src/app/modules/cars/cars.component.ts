import { Component, OnInit, OnDestroy } from '@angular/core';
import { CarService } from '../../core/services/car.service';
import { Car } from '../../shared/models/car.model';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.scss']
})
export class CarsComponent implements OnInit, OnDestroy {
  private unsubscribe: Subject<void> = new Subject();
  visibleModal = false;
  cars: Car[] = [];
  searchString = '';

  constructor(private carService: CarService, private toastr: ToastrService) { }

  ngOnInit() {
    this.getCars();
  }

  getCars() {
    this.carService.getCars()
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(cars => {
        this.cars = cars;
      });
  }

  getFiltered() {
    return this.cars.filter(car => car.name.toLocaleLowerCase().indexOf(this.searchString.toLocaleLowerCase()) >= 0);
  }

  addCar(cars: string) {
    const currentDate = new Date();
    const carsArray = cars.split('\n');

    carsArray.forEach(car => {
      const carObj = {name: car, createdDate: currentDate};
      const ifExist = this.findCar(car);

      if (car && !ifExist) {
        this.carService.createCars(carObj)
          .pipe(takeUntil(this.unsubscribe))
          .subscribe(createdCar => {
            this.cars.push(createdCar);
            this.toastr.success('Car ' + createdCar.name + ' added!');
          },
          () => {
            this.toastr.error('Add action faild, car ' + carObj.name + ' didn\'t added');
          });
      } else {
        this.toastr.error('Car ' + carObj.name + ' exist');
      }

    });

    this.toggleModal();
    cars = '';
  }

  findCar(carName: string) {
    return !!this.cars.find(el => el.name === carName);
  }

  deleteCar(car: Car) {
    this.carService.deleteCar(car.id)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(() => {
        this.cars = this.cars.filter(el => el.id !== car.id);
        this.toastr.success('Car ' + car.name +  ' was deleted');
      },
      () => {
        this.toastr.error('Delete action faild, please try again');
      });
  }

  toggleModal() {
    this.visibleModal = !this.visibleModal;
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

}
